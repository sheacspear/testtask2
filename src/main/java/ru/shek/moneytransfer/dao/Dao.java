package ru.shek.moneytransfer.dao;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Base interface for Data Access Object</br>
 * Helps to receive, update, create an object
 */
public interface Dao<ID, Data> {

    /**
     * Creating an object on the basis of a new object.
     *
     * @param account new object to create
     * @return new object
     */
    @Nonnull
    Data create(@Nonnull Data account);

    /**
     * Obtaining an object with data by ID
     *
     * @param id unique object identifier
     * @return object with data
     */
    @Nullable
    Data getById(@Nonnull ID id);

    /**
     * Updating an object on the basis of a new object. </br>
     * The search for the object for updating goes by the identifier
     *
     * @param update new object to update
     * @return Updated object
     */
    @SuppressWarnings("UnusedReturnValue")
    @Nonnull
    Data update(@Nonnull Data update);
}