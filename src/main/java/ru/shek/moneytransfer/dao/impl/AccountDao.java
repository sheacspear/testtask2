package ru.shek.moneytransfer.dao.impl;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import ru.shek.moneytransfer.dao.Dao;
import ru.shek.moneytransfer.model.Account;
import ru.shek.moneytransfer.model.impl.AccountBase;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.ThreadSafe;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Data Access Object for work with Account</br>
 * Helps to receive, update, create an object
 */
@ThreadSafe
public class AccountDao implements Dao<Long, Account> {

    /**
     * static instance for singleton
     */
    private final static AccountDao instance = new AccountDao();
    /**
     * store for Account
     */
    private final Map<Long, Account> accounts = Maps.newConcurrentMap();
    /**
     * id counter, increases for each new record Account
     */
    private final AtomicLong idCounter = new AtomicLong(0);

    /**
     * @return instance of AccountDao
     */
    public static Dao<Long, Account> getInstance() {
        return instance;
    }

    /**
     * private constructor for singleton
     */
    private AccountDao() {
    }

    @Override
    @Nonnull
    public Account create(@Nonnull Account account) {
        Preconditions.checkNotNull(account);
        AccountBase accountNew = new AccountBase(idCounter.addAndGet(1), account.getAmount());
        accounts.put(accountNew.getId(), accountNew);
        return accountNew;
    }

    @Override
    @Nullable
    public Account getById(@Nonnull Long id) {
        Preconditions.checkNotNull(id);
        return accounts.get(id);
    }

    @Override
    @Nonnull
    public Account update(@Nonnull Account account) {
        Preconditions.checkNotNull(account);
        Preconditions.checkNotNull(account.getId());
        Preconditions.checkNotNull(account.getAmount());
        accounts.put(account.getId(), account);
        return account;
    }
}
