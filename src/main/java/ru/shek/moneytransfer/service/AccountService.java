package ru.shek.moneytransfer.service;

import ru.shek.moneytransfer.model.Account;

import java.math.BigDecimal;

/**
 * Service for working with the account<br/>
 */
public interface AccountService {

    /**
     * Create an account
     *
     * @param amount on the account
     * @return object with new account
     */
    Account create(BigDecimal amount);

    /**
     * Get a balance
     *
     * @param accountId id unique object identifier of account
     * @return amount on the account
     */
    BigDecimal getBalance(Long accountId);

    /**
     * Money transfer
     *
     * @param accountFromId source of funds
     * @param accountToId   beneficiary
     * @param amount        money for transfer
     * @return operation ID
     */
    String transfer(Long accountFromId, Long accountToId, BigDecimal amount);

}
