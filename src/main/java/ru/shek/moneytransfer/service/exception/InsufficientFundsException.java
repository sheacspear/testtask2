package ru.shek.moneytransfer.service.exception;

import ru.shek.moneytransfer.model.Account;

import javax.annotation.Nonnull;
import java.math.BigDecimal;

/**
 * Insufficient Funds
 */
public class InsufficientFundsException extends RuntimeException {

    /**
     * user account
     */
    private final Account account;

    /**
     * money for transfer
     */
    private final BigDecimal amount;

    /**
     * @param account user account
     * @param amount  money for transfer
     */
    public InsufficientFundsException(@Nonnull Account account, @Nonnull BigDecimal amount) {
        this.account = account;
        this.amount = amount;
    }

    /**
     * @return user account
     */
    @Nonnull
    public Account getAccount() {
        return account;
    }

    /**
     * @return money for transfer
     */
    @Nonnull
    public BigDecimal getAmount() {
        return amount;
    }
}
