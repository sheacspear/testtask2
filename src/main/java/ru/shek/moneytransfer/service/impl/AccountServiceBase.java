package ru.shek.moneytransfer.service.impl;

import com.google.common.base.Preconditions;
import ru.shek.moneytransfer.dao.Dao;
import ru.shek.moneytransfer.dao.impl.AccountDao;
import ru.shek.moneytransfer.model.Account;
import ru.shek.moneytransfer.model.impl.AccountBase;
import ru.shek.moneytransfer.service.AccountLockManager;
import ru.shek.moneytransfer.service.AccountService;
import ru.shek.moneytransfer.service.exception.InsufficientFundsException;

import javax.annotation.Nonnull;
import javax.annotation.concurrent.ThreadSafe;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * Implementation of Service for working with the account<br/>
 */
@ThreadSafe
public class AccountServiceBase implements AccountService {

    /**
     * static instance for singleton
     */
    private final static AccountServiceBase instance = new AccountServiceBase();

    /**
     * Lock Manager for work with  Account
     */
    private final static AccountLockManager lockManager = AccountLockManagerBase.getInstance();

    /**
     * Data Access Object for work with Account
     */
    private final static Dao<Long, Account> accountDao = AccountDao.getInstance();

    /**
     * lowest possible amount for transfer
     */
    private static final BigDecimal ZERO = new BigDecimal(0);

    /**
     * private constructor for singleton
     */
    private AccountServiceBase() {
    }

    /**
     * @return instance of AccountDao
     */
    public static AccountService getInstance() {
        return instance;
    }

    @Override
    public BigDecimal getBalance(@Nonnull Long accountId) {
        Preconditions.checkNotNull(accountId);
        Account account = accountDao.getById(accountId);
        Preconditions.checkArgument(account != null);
        return account.getAmount();
    }

    @Override
    public String transfer(@Nonnull Long accountFromId, @Nonnull Long accountToId, @Nonnull BigDecimal amount) {
        Preconditions.checkNotNull(accountFromId);
        Preconditions.checkNotNull(accountToId);
        Preconditions.checkNotNull(amount);
        Preconditions.checkArgument(amount.compareTo(ZERO) > 0);
        //order blocking resource by ID
        Long blockingResourceFirst = accountFromId < accountToId ? accountFromId : accountToId;
        Long blockingResourceSecond = accountFromId < accountToId ? accountToId : accountFromId;
        try {
            lockManager.lock(blockingResourceFirst);
            try {
                lockManager.lock(blockingResourceSecond);
                Account accountFrom = accountDao.getById(accountFromId);
                Preconditions.checkArgument(accountFrom != null);
                Account accountTo = accountDao.getById(accountToId);
                Preconditions.checkArgument(accountTo != null);
                if (!validateTransfer(accountFrom, amount)) {
                    throw new InsufficientFundsException(accountFrom, amount);
                }
                accountDao.update(new AccountBase(accountFromId, accountFrom.getAmount().subtract(amount)));
                accountDao.update(new AccountBase(accountToId, accountTo.getAmount().add(amount)));
                //TODO In the future, you can save the transaction log in a separate transaction log
                return UUID.randomUUID().toString();
            } finally {
                lockManager.unlock(blockingResourceSecond);
            }
        } finally {
            lockManager.unlock(blockingResourceFirst);
        }
    }

    @Override
    public Account create(BigDecimal amount) {
        return accountDao.create(new AccountBase(null, amount));
    }

    private boolean validateTransfer(Account accountFrom, BigDecimal amount) {
        return accountFrom.getAmount().compareTo(amount) >= 1;
    }
}
