package ru.shek.moneytransfer.service.impl;

import com.google.common.collect.Maps;
import ru.shek.moneytransfer.service.AccountLockManager;

import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Implementation Lock Manager for work with  Account
 */
public class AccountLockManagerBase implements AccountLockManager {

    /**
     * static instance for singleton
     */
    private final static AccountLockManagerBase instance = new AccountLockManagerBase();

    /**
     * private constructor for singleton
     */
    private AccountLockManagerBase() {
    }

    /**
     * @return instance of AccountDao
     */
    public static AccountLockManager getInstance() {
        return instance;
    }

    /**
     * Store for outer mutex account
     * TODO add counter lock account so that the map does not expand
     */
    private final Map<Long, ReentrantLock> barrierMap = Maps.newConcurrentMap();


    @Override
    public void lock(Long accountId) {
        barrierMap.compute(accountId, (k, v) -> {
            if (v == null) {
                v = new ReentrantLock();
            }
            v.lock();
            return v;
        });
    }

    @Override
    public void unlock(Long accountId) {
        barrierMap.compute(accountId, (k, v) -> {
            if (v != null) {
                v.unlock();
            }
            return v;
        });
    }
}
