package ru.shek.moneytransfer.service;


/**
 * Lock Manager for work with  Account
 */
public interface AccountLockManager {

    /**
     * Lock account by id
     *
     * @param accountId accountId id unique object identifier of account
     */
    void lock(Long accountId);

    /**
     * Unlock account by id
     *
     * @param accountId accountId id unique object identifier of account
     */
    void unlock(Long accountId);
}
