package ru.shek.moneytransfer.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import javax.annotation.concurrent.Immutable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Result of Money transfer
 */
@Immutable
public final class TransferResult {
    /**
     * source of funds
     */
    private final Long accountFromId;
    /**
     * beneficiary
     */
    private final Long accountToId;
    /**
     * money for transfer
     */
    private final BigDecimal amount;
    /**
     * operation Id
     */
    private final String operationId;
    /**
     * operation time
     */
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private final LocalDateTime time;

    /**
     * @param accountFromId source of funds
     * @param accountToId   beneficiary
     * @param amount        money for transfer
     * @param operationId   operation Id
     */
    @JsonCreator
    public TransferResult(@JsonProperty("accountFromId") Long accountFromId,
                          @JsonProperty("accountToId") Long accountToId,
                          @JsonProperty("amount") BigDecimal amount,
                          @JsonProperty("operationId") String operationId,
                          @JsonDeserialize(using = LocalDateTimeDeserializer.class) @JsonProperty("time") LocalDateTime time

    ) {
        this.accountFromId = accountFromId;
        this.accountToId = accountToId;
        this.amount = amount;
        this.operationId = operationId;
        this.time = time;
    }

    /**
     * @return source of funds
     */
    public Long getAccountFromId() {
        return accountFromId;
    }

    /**
     * @return beneficiary
     */
    public Long getAccountToId() {
        return accountToId;
    }

    /**
     * @return money for transfer
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @return operation Id
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * @return operation time
     */
    public LocalDateTime getTime() {
        return time;
    }
}
