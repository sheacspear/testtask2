package ru.shek.moneytransfer.rest.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main exception handler for logging
 */
@Provider
public class ExceptionRestMapper implements ExceptionMapper<Throwable> {

    private static final Logger log = Logger.getLogger(ExceptionRestMapper.class.getName());

    @Override
    public Response toResponse(Throwable exception) {
        log.log(Level.SEVERE, exception.getMessage(), exception);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                .entity(exception.getMessage())
                .build();
    }
}