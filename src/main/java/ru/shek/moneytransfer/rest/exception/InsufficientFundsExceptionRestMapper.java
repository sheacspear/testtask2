package ru.shek.moneytransfer.rest.exception;

import ru.shek.moneytransfer.service.exception.InsufficientFundsException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * InsufficientFundsException exception handler<br/>
 * create error message for client
 */
@Provider
public class InsufficientFundsExceptionRestMapper implements ExceptionMapper<InsufficientFundsException> {

    private static final Logger log = Logger.getLogger(InsufficientFundsExceptionRestMapper.class.getName());

    /**
     * Error message template
     */
    private static final String INSUFFICIENT_FUNDS_FOR_TRANSFER_F_FROM_ACCOUNT_S = "Insufficient funds for transfer %f from account %s";

    @Override
    public Response toResponse(InsufficientFundsException exception) {
        log.log(Level.SEVERE, exception.getMessage(), exception);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode())
                .entity(getMessage(exception))
                .build();
    }

    private String getMessage(InsufficientFundsException exception) {
        return String.format(INSUFFICIENT_FUNDS_FOR_TRANSFER_F_FROM_ACCOUNT_S, exception.getAmount(), exception.getAccount().getId());
    }
}