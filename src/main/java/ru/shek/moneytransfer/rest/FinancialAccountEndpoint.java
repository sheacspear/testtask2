package ru.shek.moneytransfer.rest;

import ru.shek.moneytransfer.model.Account;
import ru.shek.moneytransfer.rest.dto.TransferResult;
import ru.shek.moneytransfer.service.AccountService;
import ru.shek.moneytransfer.service.impl.AccountServiceBase;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Root resource (exposed at "account" path)
 */
@Path("/account")
public class FinancialAccountEndpoint {

    /**
     * Service for working with the account
     */
    private AccountService accountService = AccountServiceBase.getInstance();

    /**
     * Create new user account
     *
     * @param amount amount on the account
     * @return user account
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Account create(@FormParam("amount") BigDecimal amount) {
        return accountService.create(amount);
    }

    /**
     * Check balance
     *
     * @param id of user account
     * @return amount on the account
     */
    @GET
    @Path("{id}/balance")
    @Produces(MediaType.TEXT_PLAIN)
    public BigDecimal balance(@PathParam("id") Long id) {
        return accountService.getBalance(id);
    }

    /**
     * Money transfer
     *
     * @param id      source of funds
     * @param accountId money for transfer
     * @param amount  amount for transfer
     * @return Result of Money transfer
     */
    @POST
    @Path("{id}/transfer")
    @Produces(MediaType.APPLICATION_JSON)
    public TransferResult transfer(@PathParam("id") Long id,
                                   @FormParam("accountId") Long accountId,
                                   @FormParam("amount") BigDecimal amount) {
        String operationId = accountService.transfer(id, accountId, amount);
        return new TransferResult(id, accountId, amount, operationId, LocalDateTime.now());
    }
}
