package ru.shek.moneytransfer.model;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;

/**
 * Interface for the work of the account
 */
public interface Account {

    /**
     * @return id unique object identifier
     */
    @Nullable
    Long getId();

    /**
     * @return amount on the account
     */
    @Nonnull
    BigDecimal getAmount();


}
