package ru.shek.moneytransfer.model.impl;

import com.google.common.base.Preconditions;
import ru.shek.moneytransfer.model.Account;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.math.BigDecimal;

/**
 * Implementation for working with the account<br/>
 */
@Immutable
public final class AccountBase implements Account {

    /**
     * lowest possible amount of the account
     */
    private static final BigDecimal ZERO = new BigDecimal(0);

    /**
     * id unique object identifier
     */
    private final Long id;

    /**
     * amount on the account
     */
    private final BigDecimal amount;

    /**
     * @param id     id unique object identifier
     * @param amount amount on the account
     */
    public AccountBase(@Nullable Long id, @Nonnull BigDecimal amount) {
        Preconditions.checkNotNull(amount);
        Preconditions.checkArgument(amount.compareTo(ZERO) >= 0);
        this.amount = amount;
        this.id = id;
    }

    @Nonnull
    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    @Nullable
    @Override
    public Long getId() {
        return id;
    }
}
