package ru.shek.moneytransfer;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.net.URI;
import java.util.Scanner;

/**
 * Application class.
 */
public class Application {
    /**
     * Base URI the Grizzly HTTP server will listen on
     */
    private static final String BASE_URI = "http://localhost:8080/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     *
     * @return Grizzly HTTP server.
     */
    private static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in ru.shek.moneytransfer
        final ResourceConfig rc = new ResourceConfig().packages("ru.shek.moneytransfer");
        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Application method.
     */
    public static void main(String[] args) {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl", BASE_URI));
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String trim = scanner.nextLine();
            System.out.println(trim);
            if ("stop".equals(trim)) {
                server.shutdown();
                break;
            }
        }
    }
}

