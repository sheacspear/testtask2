package ru.shek.moneytransfer.test;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import ru.shek.moneytransfer.rest.dto.TransferResult;
import ru.shek.moneytransfer.test.dto.AccountDto;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Test for rest service /account
 */
public class FinancialAccountBaseEndpointTest extends JerseyTest {

    @Override
    protected ResourceConfig configure() {
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new ResourceConfig().packages("ru.shek.moneytransfer.rest");
    }

    /**
     * OK: Test create account with 100 balance
     */
    @Test
    public void testCreateOK() {
        Long accountId = createAccount(new BigDecimal(100));
        assertNotNull(accountId);
    }

    /**
     * Error: Test create account with - 100 balance
     */
    @Test
    public void testCreateError() {
        Long accountId = createAccount(new BigDecimal(-100));
        assertNull(accountId);
    }

    /**
     * OK: Test get balance
     */
    @Test
    public void testBalanceOK() {
        BigDecimal amount = new BigDecimal(100);
        Long accountId = createAccount(amount);
        BigDecimal res = getBalance(accountId);
        assertEquals(amount, res);
    }


    /**
     * Error: Test get balance for not exists account
     */
    @Test
    public void testBalanceError() {
        Long accountId = -1L;
        int status = getBalanceResponse(accountId).getStatus();
        assertEquals(500, status);
    }


    /**
     * OK: test money transfer
     */
    @Test
    public void testTransferOK() {
        //init test value
        double initAmount = 100;
        BigDecimal transferAmount = new BigDecimal(50);
        //create test account
        Long accountFromId = createAccount(new BigDecimal(initAmount));
        Long accountToId = createAccount(new BigDecimal(initAmount));
        //transfer
        Response response = transferMoney(transferAmount, accountFromId, accountToId);
        TransferResult transferResult = response.readEntity(TransferResult.class);
        //check result
        assertNotNull(transferResult);
        assertNotNull(transferResult.getOperationId());
        assertNotNull(transferResult.getTime());
        assertEquals(transferResult.getAccountFromId(), accountFromId);
        assertEquals(transferResult.getAccountToId(), accountToId);
        assertEquals(transferResult.getAmount(), transferAmount);

        BigDecimal accountFromAmount = getBalance(accountFromId);
        BigDecimal accountToAmount = getBalance(accountToId);

        assertEquals(accountFromAmount, new BigDecimal(initAmount).subtract(transferAmount));
        assertEquals(accountToAmount, new BigDecimal(initAmount).add(transferAmount));
    }

    /**
     * Error: test transfer 0 amount
     */
    @Test
    public void testTransferNullAmountError() {
        //init test value
        double initAmount = 100;
        BigDecimal transferAmount = new BigDecimal(0);
        //create test account
        Long accountFromId = createAccount(new BigDecimal(initAmount));
        Long accountToId = createAccount(new BigDecimal(initAmount));
        //transfer
        Response response = transferMoney(transferAmount, accountFromId, accountToId);
        //check result
        int status = response.getStatus();
        //check result
        assertEquals(status, 500);
    }

    /**
     * Error: test transfer negative value
     */
    @Test
    public void testTransferNegativeAmountError() {
        //init test value
        double initAmount = 100;
        BigDecimal transferAmount = new BigDecimal(-50);
        //create test account
        Long accountFromId = createAccount(new BigDecimal(initAmount));
        Long accountToId = createAccount(new BigDecimal(initAmount));
        //transfer
        Response response = transferMoney(transferAmount, accountFromId, accountToId);
        //check result
        int status = response.getStatus();
        assertEquals(status, 500);
    }

    /**
     * Error: test transfer of a larger size than there is on the account
     */
    @Test
    public void testTransferLargerError() {
        //init test value
        double initAmount = 100;
        BigDecimal transferAmount = new BigDecimal(500);
        //create test account
        Long accountFromId = createAccount(new BigDecimal(initAmount));
        Long accountToId = createAccount(new BigDecimal(initAmount));
        //transfer
        Response response = transferMoney(transferAmount, accountFromId, accountToId);
        //check result
        int status = response.getStatus();
        assertEquals(status, 500);
        String message = response.readEntity(String.class);
        assertEquals(message, "Insufficient funds for transfer 500,000000 from account " + accountFromId);
    }

    private Response transferMoney(BigDecimal transferAmount, Long accountFromId, Long accountToId) {
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("accountId", String.valueOf(accountToId));
        formData.add("amount", transferAmount.toString());
        //check result
        return target("account/" + accountFromId + "/transfer").request().post(Entity.form(formData));
    }

    private Long createAccount(BigDecimal amount) {
        MultivaluedMap<String, String> formData = new MultivaluedHashMap<>();
        formData.add("amount", amount.toString());
        AccountDto account = target("account").request().post(Entity.form(formData)).readEntity(AccountDto.class);
        return account != null ? account.getId() : null;
    }

    private BigDecimal getBalance(Long accountId) {
        return getBalanceResponse(accountId).readEntity(BigDecimal.class);
    }


    private Response getBalanceResponse(Long accountId) {
        return target("account/" + accountId + "/balance").request().get();
    }
}
