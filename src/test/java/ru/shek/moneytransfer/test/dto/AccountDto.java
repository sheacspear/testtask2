package ru.shek.moneytransfer.test.dto;

import ru.shek.moneytransfer.model.Account;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigDecimal;

/**
 * Data transfer object for Account
 */
public class AccountDto implements Account {
    /**
     * id unique object identifier
     */
    private Long id;

    /**
     * amount on the account
     */
    private BigDecimal amount;

    @Nullable
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Nonnull
    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
