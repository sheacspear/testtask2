# Money Transfer Service

Service exists for creating accounts and providing transfer services between clients

## Support operations

WADL describes the service

    http://localhost:8080/myapp/application.wadl

### Create account operation

    POST /account
    Form params 'amount' is decimal account size
    Response type: application/json
    Response body example: 
        {
            id: 1,
            amount: 10000
        }

### Check balance operation

    GET /{id}/balance    
    'id' is user account id
    Response type: "text/plain"
    Response example:
        10000

### Transfer money operation

    POST {id}/transfer
    'id' is user account id
    Form params 'accountId' is beneficiary
    Form params 'amount' is money for transfer
    Response type: application/json
    Response body example: 
            {
                accountFromId: 1,
                accountToId: 2,
                amount: 1000,
                operationId: '73953058305830583058'
            }

## Build application by gradle

    ./gradlew build
    
jar file is created in the folder ./build/libs

## Run application by gradle

    ./gradlew run

## Check test
    ./gradlew test



